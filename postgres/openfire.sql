-- Adminer 4.6.2 PostgreSQL dump

\connect "openfire";

CREATE TABLE "public"."ofextcomponentconf" (
    "subdomain" character varying(255) NOT NULL,
    "wildcard" integer NOT NULL,
    "secret" character varying(255),
    "permission" character varying(10) NOT NULL,
    CONSTRAINT "ofextcomponentconf_pk" PRIMARY KEY ("subdomain")
) WITH (oids = false);

TRUNCATE "ofextcomponentconf";

CREATE TABLE "public"."ofgroup" (
    "groupname" character varying(50) NOT NULL,
    "description" character varying(255),
    CONSTRAINT "ofgroup_pk" PRIMARY KEY ("groupname")
) WITH (oids = false);

TRUNCATE "ofgroup";

CREATE TABLE "public"."ofgroupprop" (
    "groupname" character varying(50) NOT NULL,
    "name" character varying(100) NOT NULL,
    "propvalue" text NOT NULL,
    CONSTRAINT "ofgroupprop_pk" PRIMARY KEY ("groupname", "name")
) WITH (oids = false);

TRUNCATE "ofgroupprop";

CREATE TABLE "public"."ofgroupuser" (
    "groupname" character varying(50) NOT NULL,
    "username" character varying(100) NOT NULL,
    "administrator" integer NOT NULL,
    CONSTRAINT "ofgroupuser_pk" PRIMARY KEY ("groupname", "username", "administrator")
) WITH (oids = false);

TRUNCATE "ofgroupuser";

CREATE TABLE "public"."ofid" (
    "idtype" integer NOT NULL,
    "id" integer NOT NULL,
    CONSTRAINT "ofid_pk" PRIMARY KEY ("idtype")
) WITH (oids = false);

TRUNCATE "ofid";
INSERT INTO "ofid" ("idtype", "id") VALUES
(18,	1),
(19,	1),
(23,	1),
(26,	2),
(27,	1),
(25,	2);

CREATE TABLE "public"."ofmucaffiliation" (
    "roomid" integer NOT NULL,
    "jid" character varying(1024) NOT NULL,
    "affiliation" integer NOT NULL,
    CONSTRAINT "ofmucaffiliation_pk" PRIMARY KEY ("roomid", "jid")
) WITH (oids = false);

TRUNCATE "ofmucaffiliation";

CREATE TABLE "public"."ofmucconversationlog" (
    "roomid" integer NOT NULL,
    "messageid" integer NOT NULL,
    "sender" character varying(1024) NOT NULL,
    "nickname" character varying(255),
    "logtime" character(15) NOT NULL,
    "subject" character varying(255),
    "body" text,
    "stanza" text
) WITH (oids = false);

CREATE INDEX "ofmucconversationlog_msg_id" ON "public"."ofmucconversationlog" USING btree ("messageid");

CREATE INDEX "ofmucconversationlog_time_idx" ON "public"."ofmucconversationlog" USING btree ("logtime");

TRUNCATE "ofmucconversationlog";

CREATE TABLE "public"."ofmucmember" (
    "roomid" integer NOT NULL,
    "jid" character varying(1024) NOT NULL,
    "nickname" character varying(255),
    "firstname" character varying(100),
    "lastname" character varying(100),
    "url" character varying(100),
    "email" character varying(100),
    "faqentry" character varying(100),
    CONSTRAINT "ofmucmember_pk" PRIMARY KEY ("roomid", "jid")
) WITH (oids = false);

TRUNCATE "ofmucmember";

CREATE TABLE "public"."ofmucroom" (
    "serviceid" integer NOT NULL,
    "roomid" integer NOT NULL,
    "creationdate" character(15) NOT NULL,
    "modificationdate" character(15) NOT NULL,
    "name" character varying(50) NOT NULL,
    "naturalname" character varying(255) NOT NULL,
    "description" character varying(255),
    "lockeddate" character(15) NOT NULL,
    "emptydate" character(15),
    "canchangesubject" integer NOT NULL,
    "maxusers" integer NOT NULL,
    "publicroom" integer NOT NULL,
    "moderated" integer NOT NULL,
    "membersonly" integer NOT NULL,
    "caninvite" integer NOT NULL,
    "roompassword" character varying(50),
    "candiscoverjid" integer NOT NULL,
    "logenabled" integer NOT NULL,
    "subject" character varying(100),
    "rolestobroadcast" integer NOT NULL,
    "usereservednick" integer NOT NULL,
    "canchangenick" integer NOT NULL,
    "canregister" integer NOT NULL,
    "allowpm" integer,
    "fmucenabled" integer,
    "fmucoutboundnode" text,
    "fmucoutboundmode" integer,
    "fmucinboundnodes" text,
    CONSTRAINT "ofmucroom_pk" PRIMARY KEY ("serviceid", "name")
) WITH (oids = false);

CREATE INDEX "ofmucroom_roomid_idx" ON "public"."ofmucroom" USING btree ("roomid");

CREATE INDEX "ofmucroom_serviceid_idx" ON "public"."ofmucroom" USING btree ("serviceid");

TRUNCATE "ofmucroom";

CREATE TABLE "public"."ofmucroomprop" (
    "roomid" integer NOT NULL,
    "name" character varying(100) NOT NULL,
    "propvalue" text NOT NULL,
    CONSTRAINT "ofmucroomprop_pk" PRIMARY KEY ("roomid", "name")
) WITH (oids = false);

TRUNCATE "ofmucroomprop";

CREATE TABLE "public"."ofmucservice" (
    "serviceid" integer NOT NULL,
    "subdomain" character varying(255) NOT NULL,
    "description" character varying(255),
    "ishidden" integer NOT NULL,
    CONSTRAINT "ofmucservice_pk" PRIMARY KEY ("subdomain")
) WITH (oids = false);

CREATE INDEX "ofmucservice_serviceid_idx" ON "public"."ofmucservice" USING btree ("serviceid");

TRUNCATE "ofmucservice";
INSERT INTO "ofmucservice" ("serviceid", "subdomain", "description", "ishidden") VALUES
(1,	'conference',	NULL,	0);

CREATE TABLE "public"."ofmucserviceprop" (
    "serviceid" integer NOT NULL,
    "name" character varying(100) NOT NULL,
    "propvalue" text NOT NULL,
    CONSTRAINT "ofmucserviceprop_pk" PRIMARY KEY ("serviceid", "name")
) WITH (oids = false);

TRUNCATE "ofmucserviceprop";

CREATE TABLE "public"."ofoffline" (
    "username" character varying(64) NOT NULL,
    "messageid" integer NOT NULL,
    "creationdate" character(15) NOT NULL,
    "messagesize" integer NOT NULL,
    "stanza" text NOT NULL,
    CONSTRAINT "ofoffline_pk" PRIMARY KEY ("username", "messageid")
) WITH (oids = false);

TRUNCATE "ofoffline";

CREATE TABLE "public"."ofpresence" (
    "username" character varying(64) NOT NULL,
    "offlinepresence" text,
    "offlinedate" character varying(15) NOT NULL,
    CONSTRAINT "ofpresence_pk" PRIMARY KEY ("username")
) WITH (oids = false);

TRUNCATE "ofpresence";

CREATE TABLE "public"."ofprivacylist" (
    "username" character varying(64) NOT NULL,
    "name" character varying(100) NOT NULL,
    "isdefault" integer NOT NULL,
    "list" text NOT NULL,
    CONSTRAINT "ofprivacylist_pk" PRIMARY KEY ("username", "name")
) WITH (oids = false);

CREATE INDEX "ofprivacylist_default_idx" ON "public"."ofprivacylist" USING btree ("username", "isdefault");

TRUNCATE "ofprivacylist";

CREATE TABLE "public"."ofproperty" (
    "name" character varying(100) NOT NULL,
    "propvalue" character varying(4000) NOT NULL,
    "encrypted" integer,
    "iv" character(24),
    CONSTRAINT "ofproperty_pk" PRIMARY KEY ("name")
) WITH (oids = false);

TRUNCATE "ofproperty";
INSERT INTO "ofproperty" ("name", "propvalue", "encrypted", "iv") VALUES
('xmpp.socket.ssl.active',	'true',	0,	NULL),
('provider.admin.className',	'org.jivesoftware.openfire.admin.DefaultAdminProvider',	0,	NULL),
('xmpp.domain',	'c20b3a9dd5d1',	0,	NULL),
('xmpp.auth.anonymous',	'false',	0,	NULL),
('provider.auth.className',	'org.jivesoftware.openfire.auth.DefaultAuthProvider',	0,	NULL),
('provider.lockout.className',	'org.jivesoftware.openfire.lockout.DefaultLockOutProvider',	0,	NULL),
('provider.group.className',	'org.jivesoftware.openfire.group.DefaultGroupProvider',	0,	NULL),
('provider.vcard.className',	'org.jivesoftware.openfire.vcard.DefaultVCardProvider',	0,	NULL),
('provider.securityAudit.className',	'org.jivesoftware.openfire.security.DefaultSecurityAuditProvider',	0,	NULL),
('provider.user.className',	'org.jivesoftware.openfire.user.DefaultUserProvider',	0,	NULL),
('update.lastCheck',	'1640088946117',	0,	NULL);

CREATE TABLE "public"."ofpubsubaffiliation" (
    "serviceid" character varying(100) NOT NULL,
    "nodeid" character varying(100) NOT NULL,
    "jid" character varying(1024) NOT NULL,
    "affiliation" character varying(10) NOT NULL,
    CONSTRAINT "ofpubsubaffiliation_pk" PRIMARY KEY ("serviceid", "nodeid", "jid")
) WITH (oids = false);

TRUNCATE "ofpubsubaffiliation";
INSERT INTO "ofpubsubaffiliation" ("serviceid", "nodeid", "jid", "affiliation") VALUES
('pubsub',	'',	'c20b3a9dd5d1',	'owner');

CREATE TABLE "public"."ofpubsubdefaultconf" (
    "serviceid" character varying(100) NOT NULL,
    "leaf" integer NOT NULL,
    "deliverpayloads" integer NOT NULL,
    "maxpayloadsize" integer NOT NULL,
    "persistitems" integer NOT NULL,
    "maxitems" integer NOT NULL,
    "notifyconfigchanges" integer NOT NULL,
    "notifydelete" integer NOT NULL,
    "notifyretract" integer NOT NULL,
    "presencebased" integer NOT NULL,
    "senditemsubscribe" integer NOT NULL,
    "publishermodel" character varying(15) NOT NULL,
    "subscriptionenabled" integer NOT NULL,
    "accessmodel" character varying(10) NOT NULL,
    "language" character varying(255),
    "replypolicy" character varying(15),
    "associationpolicy" character varying(15) NOT NULL,
    "maxleafnodes" integer NOT NULL,
    CONSTRAINT "ofpubsubdefaultconf_pk" PRIMARY KEY ("serviceid", "leaf")
) WITH (oids = false);

TRUNCATE "ofpubsubdefaultconf";
INSERT INTO "ofpubsubdefaultconf" ("serviceid", "leaf", "deliverpayloads", "maxpayloadsize", "persistitems", "maxitems", "notifyconfigchanges", "notifydelete", "notifyretract", "presencebased", "senditemsubscribe", "publishermodel", "subscriptionenabled", "accessmodel", "language", "replypolicy", "associationpolicy", "maxleafnodes") VALUES
('pubsub',	1,	1,	10485760,	0,	1,	1,	1,	1,	0,	1,	'publishers',	1,	'open',	'English',	NULL,	'all',	-1),
('pubsub',	0,	0,	0,	0,	0,	1,	1,	1,	0,	0,	'publishers',	1,	'open',	'English',	NULL,	'all',	-1);

CREATE TABLE "public"."ofpubsubitem" (
    "serviceid" character varying(100) NOT NULL,
    "nodeid" character varying(100) NOT NULL,
    "id" character varying(100) NOT NULL,
    "jid" character varying(1024) NOT NULL,
    "creationdate" character(15) NOT NULL,
    "payload" text,
    CONSTRAINT "ofpubsubitem_pk" PRIMARY KEY ("serviceid", "nodeid", "id")
) WITH (oids = false);

TRUNCATE "ofpubsubitem";

CREATE TABLE "public"."ofpubsubnode" (
    "serviceid" character varying(100) NOT NULL,
    "nodeid" character varying(100) NOT NULL,
    "leaf" integer NOT NULL,
    "creationdate" character(15) NOT NULL,
    "modificationdate" character(15) NOT NULL,
    "parent" character varying(100),
    "deliverpayloads" integer NOT NULL,
    "maxpayloadsize" integer,
    "persistitems" integer,
    "maxitems" integer,
    "notifyconfigchanges" integer NOT NULL,
    "notifydelete" integer NOT NULL,
    "notifyretract" integer NOT NULL,
    "presencebased" integer NOT NULL,
    "senditemsubscribe" integer NOT NULL,
    "publishermodel" character varying(15) NOT NULL,
    "subscriptionenabled" integer NOT NULL,
    "configsubscription" integer NOT NULL,
    "accessmodel" character varying(10) NOT NULL,
    "payloadtype" character varying(100),
    "bodyxslt" character varying(100),
    "dataformxslt" character varying(100),
    "creator" character varying(1024) NOT NULL,
    "description" character varying(255),
    "language" character varying(255),
    "name" character varying(50),
    "replypolicy" character varying(15),
    "associationpolicy" character varying(15),
    "maxleafnodes" integer,
    CONSTRAINT "ofpubsubnode_pk" PRIMARY KEY ("serviceid", "nodeid")
) WITH (oids = false);

TRUNCATE "ofpubsubnode";
INSERT INTO "ofpubsubnode" ("serviceid", "nodeid", "leaf", "creationdate", "modificationdate", "parent", "deliverpayloads", "maxpayloadsize", "persistitems", "maxitems", "notifyconfigchanges", "notifydelete", "notifyretract", "presencebased", "senditemsubscribe", "publishermodel", "subscriptionenabled", "configsubscription", "accessmodel", "payloadtype", "bodyxslt", "dataformxslt", "creator", "description", "language", "name", "replypolicy", "associationpolicy", "maxleafnodes") VALUES
('pubsub',	'',	0,	'001640088905822',	'001640088905822',	NULL,	0,	0,	0,	0,	1,	1,	1,	0,	0,	'publishers',	1,	0,	'open',	'',	'',	'',	'c20b3a9dd5d1',	'',	'English',	'',	NULL,	'all',	-1);

CREATE TABLE "public"."ofpubsubnodegroups" (
    "serviceid" character varying(100) NOT NULL,
    "nodeid" character varying(100) NOT NULL,
    "rostergroup" character varying(100) NOT NULL
) WITH (oids = false);

CREATE INDEX "ofpubsubnodegroups_idx" ON "public"."ofpubsubnodegroups" USING btree ("serviceid", "nodeid");

TRUNCATE "ofpubsubnodegroups";

CREATE TABLE "public"."ofpubsubnodejids" (
    "serviceid" character varying(100) NOT NULL,
    "nodeid" character varying(100) NOT NULL,
    "jid" character varying(1024) NOT NULL,
    "associationtype" character varying(20) NOT NULL,
    CONSTRAINT "ofpubsubnodejids_pk" PRIMARY KEY ("serviceid", "nodeid", "jid")
) WITH (oids = false);

TRUNCATE "ofpubsubnodejids";

CREATE TABLE "public"."ofpubsubsubscription" (
    "serviceid" character varying(100) NOT NULL,
    "nodeid" character varying(100) NOT NULL,
    "id" character varying(100) NOT NULL,
    "jid" character varying(1024) NOT NULL,
    "owner" character varying(1024) NOT NULL,
    "state" character varying(15) NOT NULL,
    "deliver" integer NOT NULL,
    "digest" integer NOT NULL,
    "digest_frequency" integer NOT NULL,
    "expire" character(15),
    "includebody" integer NOT NULL,
    "showvalues" character varying(30) NOT NULL,
    "subscriptiontype" character varying(10) NOT NULL,
    "subscriptiondepth" integer NOT NULL,
    "keyword" character varying(200),
    CONSTRAINT "ofpubsubsubscription_pk" PRIMARY KEY ("serviceid", "nodeid", "id")
) WITH (oids = false);

TRUNCATE "ofpubsubsubscription";

CREATE TABLE "public"."ofremoteserverconf" (
    "xmppdomain" character varying(255) NOT NULL,
    "remoteport" integer,
    "permission" character varying(10) NOT NULL,
    CONSTRAINT "ofremoteserverconf_pk" PRIMARY KEY ("xmppdomain")
) WITH (oids = false);

TRUNCATE "ofremoteserverconf";

CREATE TABLE "public"."ofroster" (
    "rosterid" integer NOT NULL,
    "username" character varying(64) NOT NULL,
    "jid" character varying(1024) NOT NULL,
    "sub" integer NOT NULL,
    "ask" integer NOT NULL,
    "recv" integer NOT NULL,
    "nick" character varying(255),
    CONSTRAINT "ofroster_pk" PRIMARY KEY ("rosterid")
) WITH (oids = false);

CREATE INDEX "ofroster_jid_idx" ON "public"."ofroster" USING btree ("jid");

CREATE INDEX "ofroster_username_idx" ON "public"."ofroster" USING btree ("username");

TRUNCATE "ofroster";

CREATE TABLE "public"."ofrostergroups" (
    "rosterid" integer NOT NULL,
    "rank" integer NOT NULL,
    "groupname" character varying(255) NOT NULL,
    CONSTRAINT "ofrostergroups_pk" PRIMARY KEY ("rosterid", "rank"),
    CONSTRAINT "ofrostergroups_rosterid_fk" FOREIGN KEY (rosterid) REFERENCES ofroster(rosterid) DEFERRABLE INITIALLY DEFERRED DEFERRABLE
) WITH (oids = false);

CREATE INDEX "ofrostergroups_rosterid_idx" ON "public"."ofrostergroups" USING btree ("rosterid");

TRUNCATE "ofrostergroups";

CREATE TABLE "public"."ofsaslauthorized" (
    "username" character varying(64) NOT NULL,
    "principal" character varying(4000) NOT NULL,
    CONSTRAINT "ofsaslauthorized_pk" PRIMARY KEY ("username", "principal")
) WITH (oids = false);

TRUNCATE "ofsaslauthorized";

CREATE TABLE "public"."ofsecurityauditlog" (
    "msgid" integer NOT NULL,
    "username" character varying(64) NOT NULL,
    "entrystamp" bigint NOT NULL,
    "summary" character varying(255) NOT NULL,
    "node" character varying(255) NOT NULL,
    "details" text,
    CONSTRAINT "ofsecurityauditlog_pk" PRIMARY KEY ("msgid")
) WITH (oids = false);

CREATE INDEX "ofsecurityauditlog_tstamp_idx" ON "public"."ofsecurityauditlog" USING btree ("entrystamp");

CREATE INDEX "ofsecurityauditlog_uname_idx" ON "public"."ofsecurityauditlog" USING btree ("username");

TRUNCATE "ofsecurityauditlog";
INSERT INTO "ofsecurityauditlog" ("msgid", "username", "entrystamp", "summary", "node", "details") VALUES
(1,	'admin',	1640088914428,	'Successful admin console login attempt',	'c20b3a9dd5d1',	'The user logged in successfully to the admin console from address 192.168.5.15. ');

CREATE TABLE "public"."ofuser" (
    "username" character varying(64) NOT NULL,
    "storedkey" character varying(32),
    "serverkey" character varying(32),
    "salt" character varying(32),
    "iterations" integer,
    "plainpassword" character varying(32),
    "encryptedpassword" character varying(255),
    "name" character varying(100),
    "email" character varying(100),
    "creationdate" character(15) NOT NULL,
    "modificationdate" character(15) NOT NULL,
    CONSTRAINT "ofuser_pk" PRIMARY KEY ("username")
) WITH (oids = false);

CREATE INDEX "ofuser_cdate_idx" ON "public"."ofuser" USING btree ("creationdate");

TRUNCATE "ofuser";
INSERT INTO "ofuser" ("username", "storedkey", "serverkey", "salt", "iterations", "plainpassword", "encryptedpassword", "name", "email", "creationdate", "modificationdate") VALUES
('admin',	NULL,	NULL,	NULL,	NULL,	'admin',	NULL,	'Administrator',	'admin@example.com',	'0              ',	'0              ');

CREATE TABLE "public"."ofuserflag" (
    "username" character varying(64) NOT NULL,
    "name" character varying(100) NOT NULL,
    "starttime" character(15),
    "endtime" character(15),
    CONSTRAINT "ofuserflag_pk" PRIMARY KEY ("username", "name")
) WITH (oids = false);

CREATE INDEX "ofuserflag_etime_idx" ON "public"."ofuserflag" USING btree ("endtime");

CREATE INDEX "ofuserflag_stime_idx" ON "public"."ofuserflag" USING btree ("starttime");

TRUNCATE "ofuserflag";

CREATE TABLE "public"."ofuserprop" (
    "username" character varying(64) NOT NULL,
    "name" character varying(100) NOT NULL,
    "propvalue" text NOT NULL,
    CONSTRAINT "ofuserprop_pk" PRIMARY KEY ("username", "name")
) WITH (oids = false);

TRUNCATE "ofuserprop";

CREATE TABLE "public"."ofvcard" (
    "username" character varying(64) NOT NULL,
    "vcard" text NOT NULL,
    CONSTRAINT "ofvcard_pk" PRIMARY KEY ("username")
) WITH (oids = false);

TRUNCATE "ofvcard";

CREATE TABLE "public"."ofversion" (
    "name" character varying(50) NOT NULL,
    "version" integer NOT NULL,
    CONSTRAINT "ofversion_pk" PRIMARY KEY ("name")
) WITH (oids = false);

TRUNCATE "ofversion";
INSERT INTO "ofversion" ("name", "version") VALUES
('openfire',	32);

-- 2021-12-21 12:16:49.334254+00
