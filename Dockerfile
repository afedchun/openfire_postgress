FROM ubuntu:18.04
LABEL mainteiner="Artem Fedchun afedchun88@gmail.com"
WORKDIR /var/lib/postgresql
## Copy backuped data for openfire db
COPY openfire.sql /var/lib/postgresql
## Update dependensies and install psql  
RUN apt-get update && apt -y install postgresql postgresql-contrib
## Create config files for correct working psql and  fill up them
RUN touch /var/lib/postgresql/10/main/pg_hba.conf \
  && touch /var/lib/postgresql/10/main/pg_ident.conf \
  && echo "host  all  all 0.0.0.0/0 md5" >> /var/lib/postgresql/10/main/pg_hba.conf \
  && echo "local  all  postgres ident" >> /var/lib/postgresql/10/main/pg_hba.conf \
  && echo "local  openfire  openfire trust" >> /var/lib/postgresql/10/main/pg_hba.conf 
## Use psql user
USER postgres
## Create and fill up main config file
RUN cp  /var/lib/postgresql/10/main/postgresql.auto.conf /var/lib/postgresql/10/main/postgresql.conf \
  && echo "listen_addresses = '*'" >> /var/lib/postgresql/10/main/postgresql.conf
## Create user, db for openfire and import backuped db
RUN /usr/lib/postgresql/10/bin/pg_ctl -D /var/lib/postgresql/10/main start \
  && psql -c "create user openfire with password '123123';" \
  && psql -c "create database openfire;" && psql -U openfire -d openfire -f /var/lib/postgresql/openfire.sql \
  && psql -c "GRANT ALL PRIVILEGES ON DATABASE "openfire" to openfire;"

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/postgresql/10/bin


ENV PGDATA=/var/lib/postgresql/10/main/


CMD ["postgres"]


																 
